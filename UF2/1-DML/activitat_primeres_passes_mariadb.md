# Activitat: Primeres passes amb MariaDB

## Instal·lació

La instal·lació en Debian GNU/Linux és molt senzilla: només cal instal·lar
el paquet **mariadb-server**, des del Synaptic o des de la línia d'ordres:

```bash
$ sudo apt-get update
$ sudo apt-get install mariadb-server
```

Durant la instal·lació se'ns demanarà la contrasenya per l'usuari _root_ de
MariaDB. L'usuari _root_ és l'administrador del SGBD, amb permisos per fer
qualsevol operació, i no té res a veure amb l'usuari _root_ del sistema
operatiu.

Un cop instal·lat el servidor podem comprovar que s'està executant amb:

```bash
$ systemctl status mysql
● mysql.service - LSB: Start and stop the mysql database server daemon
   Loaded: loaded (/etc/init.d/mysql)
   Active: active (running) since dt 2016-09-20 10:38:14 CEST; 8min ago
  Process: 711 ExecStart=/etc/init.d/mysql start (code=exited, status=0/SUCCESS)
   CGroup: /system.slice/mysql.service
           ├─820 /bin/bash /usr/bin/mysqld_safe
           ├─821 logger -p daemon.err -t /etc/init.d/mysql -i
           ├─974 /usr/sbin/mysqld --basedir=/usr --datadir=/var/lib/mysql --p...
           └─975 logger -t mysqld -p daemon.error
```

És convenient assegurar la instal·lació per evitar problemes evidents de
seguretat, com que l'usuari _root_ pugui connectar des d'altres màquines. Per
fer-ho, executarem l'script **mysql_secure_installation**.

Aquest script ens demanarà si volem canviar la contrasenya de _root_ (no, ja
li hem assignat una durant la instal·lació), i ens proposarà una colla d'altres
operacions a les quals podem contestar que sí:

```
$ sudo mysql_secure_installation

NOTE: RUNNING ALL PARTS OF THIS SCRIPT IS RECOMMENDED FOR ALL MariaDB
      SERVERS IN PRODUCTION USE!  PLEASE READ EACH STEP CAREFULLY!

[...]

Enter current password for root (enter for none):
OK, successfully used password, moving on...

[...]

Change the root password? [Y/n] n
 ... skipping.

[...]

Remove anonymous users? [Y/n]

... Success!

[...]

Disallow root login remotely? [Y/n]
... Success!

[...]

Remove test database and access to it? [Y/n]
- Dropping test database...
... Success!
- Removing privileges on test database...
... Success!

[...]

Reload privilege tables now? [Y/n]
... Success!

[...]
```

## Connexió al servidor en línia d'ordres

Per connectar amb el servidor utilitzarem l'ordre següent:

```bash
$ mysql -u root -p
```

L'opció *-u* permet indicar amb quin usuari connectem, i l'opció *-p* indica
que utilitzarem una contrasenya per autenticar-nos en el servidor.

A continuació es presenten algunes ordres bàsiques per moure-ns en el sistema.

- `show databases;`: mostra una llista amb totes les bases de dades disponibles
al servidor. Com a mínim trobarem les bases de dades *mysql* (on es guarda la
informació de tots els usuaris i permisos del servidor), *information_schema*
i *performance_schema*.

- `use <base de dades>;`: passem a utilitzar una de les bases de dades que hi
ha en el SGBD.

- `show tables;`: un cop estem utilitzant una base de dades concreta, amb
aquesta ordre podrem veure totes les taules que hi ha en aquesta BD.

- `desc <taula>;`: amb aquesta ordre podem veure l'estructura d'una de les
taules: quines columnes té i de quin tipus són, entre d'altres propietats.

- `select * from <taula>;`: aquesta és la primera sentència pròpiament SQL que
executem. Mostrarà el continugut complet de la taula que indiquem.

- `exit`: amb *exit* o amb Ctrl+D tanquem la connexió al servidor.

Cal tenir en compte que la major part d'ordres han d'acabar en punt i coma. Si
no es posa, el sistema pensarà que encara no hem escrit l'ordre sencera, ja que
és habitual que les ordres SQL ocupin més d'una línia. Llavors, tenim prou amb
escriure el punt i coma que ens hem deixat a la línia següent per poder
continuar:

```sql
MariaDB [mysql]> select * from user
    -> ;
```

## Connexió al servidor des de l'entorn gràfic

Per connectar-nos gràficament al servidor de MariaDB podem utilitzar un
programa anomenat **mysql-workbench**, disponible al dipòsits de Debian:

```
$ sudo apt-get install mysql-workbench
```

El Workbench ens permet configurar diverses connexions i recordar-les per més
endavant:

![Pantalla incial del Workbench](images/workbench1.png)

Si la connexió al servidor local no apareix, l'hem d'afegir. Les dades que
apareixen  per defecte (servidor 127.0.0.1, port 3306, usuari root) són ja les
correctes. Només cal posar-li un nom a la connexió i escriure la contrasenya
quan connectem.

Un cop hem connectat podem veure a l'esquerra totes les bases de dades que hi
ha al servidor i podem passar a utilitzar-ne una amb un doble clic a sobre del
seu nom.

A la finestra central podem escriure consultes SQL i executar-les amb el botó
en què apareix un llamp dibuixat:

![Consulta SQL al Workbench](images/workbench2.png)

El Workbench admet moltes altres possibilitats, algunes de les quals veurem
més endavant.

## Importació d'una base de dades

Per començar a treballar amb l'SQL podem fer-ho amb la base de dades d'exemple
sobre [cognoms catalans](lastnames.sql). Cal baixar aquest fitxer en local i
importar-lo a MariaDB.

Si esteu treballant des de la web del GitLab podeu baixar-vos el fitxer clicant
amb el botó dret sobre de *raw* i indicant que voleu guardar el fitxer.

Suposem que l'hem guardat a *Baixades*. Si el meu usuari és *joan*, la ruta
completa a aquest fitxer serà `/home/joan/Baixades/lastnames.sql`.

Per importar la base de dades seguim els següents passos:

```
$ mysql -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 44
Server version: 10.0.27-MariaDB-0+deb8u1 (Debian)

Copyright (c) 2000, 2016, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> create database lastnames;
Query OK, 1 row affected (0.00 sec)

MariaDB [(none)]> use lastnames;
Database changed
MariaDB [lastnames]> source /home/joan/Baixades/lastnames.sql
Query OK, 0 rows affected (0.00 sec)
[...]

Query OK, 29980 rows affected (2.73 sec)
Records: 29980  Duplicates: 0  Warnings: 0

Query OK, 11972 rows affected (0.22 sec)
Records: 11972  Duplicates: 0  Warnings: 0
[...]
```

L'ordre `create database` crea una nova base de dades i l'ordre `source`
permet executar un script SQL guardat a un fitxer.

## Primeres sentències SQL

Amb una primera base de dades funcional, ja podem començar a realitzar les
nostres primeres consultes.

Aquesta base de dades conté una única taula, anomenada _LastNames_. Aquesta
taula conté informació sobre la freqüència d'aparició dels diferents cognoms
en la població de Catalunya el 2014.

Les seves columnes són les següents:

- `LastNameId`: un identificador únic per cada cognom que, a més, ens indica la
posició relativa del cognom a la llista. Com més baix, més freqüent és el
cognom.
- `Name`: el cognom en sí.
- `Freq1`: la quantitat de persones que tenen aquest cognom com a primer cognom.
- `Freq1PerThousand`: sobre mil persones, quantes persones tindrien aquest
cognom com a primer cognom.
- `Freq2`: com _Freq1_ però pel segon cognom.
- `Freq2PerThousand`: com _Freq1PerThousand_ però pel segon cognom.

Algunes sentències que podem provar:

- Veure la informació sobre 10 cognoms qualsevols (en aquest cas surten
ordenats, però la sentència en general no ho està garantint):

```sql
SELECT * FROM LastNames LIMIT 10;
```

- Veure la informació sobre un cognom concret:

```sql
SELECT * FROM LastNames WHERE Nom="Puig";
```

- Veure la informació dels cognoms pels qual hi ha més de 20000 persones que el
tenen com a primer cognom:

```sql
SELECT * FROM LastNames WHERE Freq1>20000;
```
